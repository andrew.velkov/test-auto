## test auto

### `git clone git@gitlab.com:andrew.velkov/test-auto.git`

### `cd test-auto`

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode to view it in the browser.
Open App [http://localhost:3001](http://localhost:3001)

---

Test Jest

### `npm run test`

---

Development Build:
### `npm run build:dev`

---

Production Build:
### `npm run build`

---

Automatically formatting code with prettier / eslint and running JEST tests with husky.hooks - pre-commit / pre-push