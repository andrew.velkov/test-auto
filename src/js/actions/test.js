import * as type from 'constants/test';

export const getTerms = (params) => ({
  types: [type.GET_TERMS_REQUEST, type.GET_TERMS_SUCCESS, type.GET_TERMS_FAILURE],
  payload: {
    request: {
      url: '/search/terms',
      method: 'GET',
    },
  },
  params,
});

export const getBrandsAndTerms = (params) => ({
  types: [type.GET_BRANDS_TERMS_REQUEST, type.GET_BRANDS_TERMS_SUCCESS, type.GET_BRANDS_TERMS_FAILURE],
  payload: {
    request: {
      url: '/search/brands_terms',
      method: 'GET',
    },
  },
  params,
});

export const getStyles = (params) => ({
  types: [type.GET_STYLES_REQUEST, type.GET_STYLES_SUCCESS, type.GET_STYLES_FAILURE],
  payload: {
    request: {
      url: '/search/styles',
      method: 'GET',
    },
  },
  params,
});
