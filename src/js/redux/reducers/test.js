const initialState = {
  termsData: [],
  termsDataLoaded: false,
  termsDataLoading: false,

  brandTermsData: [],
  brandTermsDataLoaded: false,
  brandTermsDataLoading: false,
  
  stylesData: [],
  stylesDataLoaded: false,
  stylesDataLoading: false,
};

export default function test(param = '') {
  return (state = initialState, action = {}) => {
    const params = action.params || (action.meta && action.meta.previousAction.params);

    switch (action.type) {
      case `test/${param}/REQUEST`:
        return {
          ...state,
          [`${params}Loading`]: true,
          [`${params}Loaded`]: false,
        };
      case `test/${param}/SUCCESS`:
        return {
          ...state,
          [`${params}Loading`]: false,
          [`${params}Loaded`]: true,
          [params]: action.payload && action.payload.data.data,
        };
      case `test/${param}/FAILURE`:
        return {
          ...state,
          [`${params}Loading`]: false,
          [`${params}Loaded`]: false,
        };
      default:
        return state;
    }
  };
}
