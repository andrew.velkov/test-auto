import { combineReducers } from 'redux';

// other
import notification from './notification';
import test from './test';

const reducers = combineReducers({
  get: combineReducers({
    notifications: notification('notification'),
    terms: test('terms'),
    brandTerms: test('brandTerms'),
    styles: test('styles'),
  }),
});

export default reducers;
