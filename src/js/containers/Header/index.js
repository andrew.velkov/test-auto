import React from 'react';
import cx from 'classnames';

import css from 'styles/containers/Header.scss';

const HeaderContainer = () => (
  <header className={cx(css.header, css.header_global)}>
    <div className={css.header__container}>
      <h4>TEST</h4>
    </div>
  </header>
);


export default HeaderContainer;
