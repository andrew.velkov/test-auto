import React from 'react';

import Header from 'containers/Header';
import Footer from 'containers/Footer';

import css from 'styles/containers/Member.scss';

const Member = ({ children, ...props }) => (
  <section className={css.member}>
    <article className={css.member__flex}>
      <section className={css.member__wrap}>
        <div className={css.member__container}>
          <Header {...props} />
          <section className={css.member__content}>
            {children}
          </section>
          <Footer />
        </div>
      </section>
    </article>
  </section>
);

export default Member;
