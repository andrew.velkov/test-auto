import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid } from '@material-ui/core';
import VirtualizedSelect from 'react-virtualized-select';

import 'styles/components/test.scss';

import { getTerms, getBrandsAndTerms, getStyles } from 'actions';
import Fetching from 'components/Fetching';

const HomeContainer = (props) => {
  const initialList = {
    terms: null,
    brandsAndTerms: null,
    styles: null,
  };
  const [list, setList] = useState(initialList);
  const dispatch = useDispatch();

  const loadedTerms = (params) => dispatch(getTerms(params));
  const loadBrandsAndTerms = (params) => dispatch(getBrandsAndTerms(params));
  const loadedStyles = (params) => dispatch(getStyles(params));

  const { terms, brandsAndTerms, styles } = list;

  const initialPath = () => {
    const pathname = window.location.pathname.split('/').filter((i) => i !== '');
    const initialData = {
      terms: (pathname[0] && pathname[0].replace('s-', '')) || null,
      brandsAndTerms: (pathname[1] && pathname[1].replace('b-', '')) || null,
      styles: (pathname[2] && pathname[2].replace('st-', ''))|| null,
    };
    setList({ ...list, ...initialData });
  }

  useEffect(() => {
    initialPath();
    loadedTerms('termsData');
    loadedStyles('stylesData');
    loadBrandsAndTerms('brandTermsData');
  }, []);

  useEffect(() => {
    const data = {
      terms: terms ? `/s-${terms}`: '',
      brandsAndTerms: brandsAndTerms ? `/b-${brandsAndTerms}`: '',
      styles: styles ? `/st-${styles}`: '',
    };

    const path = `${data.terms}${data.brandsAndTerms}${data.styles}`;

    const location = { pathname: '' };
    props.history.replace(location);
    props.history.push(path);

  }, [terms, brandsAndTerms, styles]);

  const listData = () => {
    const { termsData, termsDataLoading, termsDataLoaded } = useSelector((state) => state.get.terms);
    const { brandTermsData, brandTermsDataLoaded, brandTermsDataLoading } = useSelector((state) => state.get.brandTerms);
    const { stylesData, stylesDataLoaded, stylesDataLoading } = useSelector((state) => state.get.styles);

    return {
      termsData, termsDataLoading, termsDataLoaded, brandTermsData, brandTermsDataLoaded,
      brandTermsDataLoading, stylesData, stylesDataLoaded, stylesDataLoading,
    };
  };

  const handleChange = (item, name) => {
    setList({ ...list, [name]: item.value });
  };

  const {
    termsData, termsDataLoading, termsDataLoaded, brandTermsData, brandTermsDataLoaded, stylesData,
    stylesDataLoaded,
  } = listData();

  const options = (option) => Array.from(option, (item) => ({
    value: item.slug,
    label: item.label,
  }));

  return (
    <Fetching isFetching={!termsData.length || termsDataLoading}>
      {termsDataLoaded && brandTermsDataLoaded && stylesDataLoaded && (
      <Grid container spacing={2}>
        <Grid item xs={12} sm={4}>
          <VirtualizedSelect
            options={options(termsData)}
            value={terms}
            onChange={(selectValue) => handleChange(selectValue, 'terms')}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <VirtualizedSelect
            options={options(brandTermsData)}
            value={brandsAndTerms}
            onChange={(selectValue) => handleChange(selectValue, 'brandsAndTerms')}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <VirtualizedSelect
            options={options(stylesData)}
            value={styles}
            onChange={(selectValue) => handleChange(selectValue, 'styles')}
          />
        </Grid>
      </Grid>
      )}
    </Fetching>
  );
}

export default HomeContainer;
