const API = {
  baseUrl: 'https://beta.autobooking.com/api/test/v1',
  redirect_uri: window.location.origin,
};

export default API;
