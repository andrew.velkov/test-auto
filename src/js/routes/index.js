import React, { Suspense, lazy, useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { langLoad } from 'translations';

import ErrorBoundary from 'containers/ErrorBoundary';
import Loader from 'components/Loader';

const App = lazy(() => import('containers/App'));
const MemberLayout = lazy(() => import('containers/Member'));
const Home = lazy(() => import('pages/Home'));

const Routes = () => {
  useEffect(() => {
    langLoad();
  }, []);

  return (
    <BrowserRouter>
      <Suspense fallback={<Loader isFetching />}>
        <App>
          <MemberLayout>
            <ErrorBoundary>
              <Switch>
                <Route exact path="/" render={(props) => <Home {...props} />} />
                <Route path="/*" render={(props) => <Home {...props} />} />
              </Switch>
            </ErrorBoundary>
          </MemberLayout>
        </App>
      </Suspense>
    </BrowserRouter>
  );
}

export default Routes;
