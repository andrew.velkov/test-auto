
export const GET_TERMS_REQUEST = 'test/terms/REQUEST';
export const GET_TERMS_SUCCESS = 'test/terms/SUCCESS';
export const GET_TERMS_FAILURE = 'test/terms/FAILURE ';

export const GET_BRANDS_TERMS_REQUEST = 'test/brandTerms/REQUEST';
export const GET_BRANDS_TERMS_SUCCESS = 'test/brandTerms/SUCCESS';
export const GET_BRANDS_TERMS_FAILURE = 'test/brandTerms/FAILURE ';

export const GET_STYLES_REQUEST = 'test/styles/REQUEST';
export const GET_STYLES_SUCCESS = 'test/styles/SUCCESS';
export const GET_STYLES_FAILURE = 'test/styles/FAILURE ';
